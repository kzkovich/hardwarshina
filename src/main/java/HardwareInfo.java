import oshi.SystemInfo;
import oshi.hardware.GraphicsCard;
import oshi.hardware.HWDiskStore;
import oshi.hardware.HardwareAbstractionLayer;

import java.util.ArrayList;

public class HardwareInfo {
    private String computerManufacturer;
    private String computerModel;

    private String cpuIdentifier;
    private String cpuModel;
    private String cpuName;

    private long ramTotal;
    private long ramAvailable;

    GraphicsCard[] gpu;
    private ArrayList<GPUDetails> gpuList = new ArrayList<>();

    HWDiskStore[] storage;
    private ArrayList<HardwareDetails> hardwareList = new ArrayList<>();

    public String getComputerManufacturer() {
        return computerManufacturer;
    }

    public String getComputerModel() {
        return computerModel;
    }

    public String getCpuIdentifier() {
        return cpuIdentifier;
    }

    public String getCpuModel() {
        return cpuModel;
    }

    public String getCpuName() {
        return cpuName;
    }

    public long getRamTotal() {
        return ramTotal;
    }

    public long getRamAvailable() {
        return ramAvailable;
    }

    public ArrayList<GPUDetails> getGpuList() {
        return gpuList;
    }

    public ArrayList<HardwareDetails> getHardwareList() {
        return hardwareList;
    }

    HardwareInfo() {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        this.computerManufacturer = hal.getComputerSystem().getManufacturer();
        this.computerModel = hal.getComputerSystem().getModel();

        this.cpuIdentifier = hal.getProcessor().getProcessorIdentifier().getIdentifier();
        this.cpuModel = hal.getProcessor().getProcessorIdentifier().getModel();
        this.cpuName = hal.getProcessor().getProcessorIdentifier().getName();

        this.ramTotal = hal.getMemory().getTotal();
        this.ramAvailable = hal.getMemory().getAvailable();

        this.gpu = hal.getGraphicsCards();
        for (GraphicsCard graphicsCard : gpu) {
            this.gpuList.add(
                    new GPUDetails(
                            graphicsCard.getName(),
                            graphicsCard.getVRam()
                    )
            );
        }

        this.storage = hal.getDiskStores();
        for (HWDiskStore hardwares : storage) {
            this.hardwareList.add(
                    new HardwareDetails(
                            hardwares.getModel(),
                            hardwares.getName(),
                            hardwares.getSize()
                    )
            );
        }

    }

    class GPUDetails {
        private String name;
        private long vRAm;

        private GPUDetails(String name, long vRam) {
            this.name = name;
            this.vRAm = vRam;
        }

        String getName() {
            return name;
        }

        long getVram() {
            return vRAm;
        }
    }

    class HardwareDetails {
        private String model;
        private String name;
        long size;

        private HardwareDetails(String model, String name, long size) {
            this.model = model;
            this.name = name;
            this.size = size;
        }

        String getModel() {
            return model;
        }

        String getName() {
            return name;
        }

        long getSize() {
            return size;
        }
    }
}
