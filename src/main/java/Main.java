import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class Main extends Application {
//    String logoPath = "src/main/resources/zel.jpg";

    @Override
    public void start(Stage primaryStage) throws Exception{
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(20);
        hBox.getChildren().add(initiatePlayer());
        hBox.getChildren().add(populateVBox());
        Scene scene = new Scene(hBox, 1024, 768);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Hardwarushka");
//        primaryStage.getIcons().add(new Image(String.valueOf(new File(logoPath).toURL())));
        primaryStage.show();
    }

    private MediaView initiatePlayer() throws URISyntaxException {
        URL videoPath = getClass().getResource("/gendalf.mp4");
//        Media media = new Media(new File(videoPath.toURI().toString()));
        Media media = new Media(videoPath.toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.seek(Duration.ZERO));
        return new MediaView(mediaPlayer);
    }

    private VBox populateVBox() {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER_LEFT);
        HardwareInfo hardwareInfo = new HardwareInfo();
        ArrayList<HardwareInfo.GPUDetails> gpus = hardwareInfo.getGpuList();
        ArrayList<HardwareInfo.HardwareDetails> hardwares = hardwareInfo.getHardwareList();

        Label hLineLabel = new Label("--------------------------");
        Label hLineLabel2 = new Label("--------------------------");
        Label hLineLabel3 = new Label("--------------------------");
        Label hLineLabel4 = new Label("--------------------------");
        Label computerInfoLabel = new Label("Computer Information");
        Label computerManufacturerLabel = new Label("Manufacturer: " + hardwareInfo.getComputerManufacturer());
        Label computerModelLabel = new Label("Model: " + hardwareInfo.getComputerModel());
        Label cpuInfoLabel = new Label("CPU Information");
        Label cpuIdentifierLabel = new Label("CPU Identifier: " + hardwareInfo.getCpuIdentifier());
        Label cpuModelLabel = new Label("CPU Model: " + hardwareInfo.getCpuModel());
        Label cpuNameLabel = new Label("CPU Name: " + hardwareInfo.getCpuName());
        Label ramInfoLabel = new Label("RAM Information");
        Label ramTotalLabel = new Label("RAM Total: " + hardwareInfo.getRamTotal());
        Label ramAvailableLabel = new Label("RAM Available: " + hardwareInfo.getRamAvailable());
        Label gpuInfoLabel = new Label("GPU Information");
        Label hardwareInfoLabel = new Label("Disk Information");

        vBox.getChildren().add(computerInfoLabel);
        vBox.getChildren().add(computerManufacturerLabel);
        vBox.getChildren().add(computerModelLabel);
        vBox.getChildren().add(hLineLabel);
        vBox.getChildren().add(cpuInfoLabel);
        vBox.getChildren().add(cpuIdentifierLabel);
        vBox.getChildren().add(cpuModelLabel);
        vBox.getChildren().add(cpuNameLabel);
        vBox.getChildren().add(hLineLabel2);
        vBox.getChildren().add(ramInfoLabel);
        vBox.getChildren().add(ramTotalLabel);
        vBox.getChildren().add(ramAvailableLabel);
        vBox.getChildren().add(hLineLabel3);
        vBox.getChildren().add(gpuInfoLabel);

        if (gpus.size() <= 0) {
            Label gpuNameLabel = new Label("GPU не найден");
            vBox.getChildren().add(gpuNameLabel);
        } else {
            for (int i = 0; i < gpus.size(); i++) {
                Label gpuNameLabel = new Label("GPU " + i + " name: " + gpus.get(i).getName());
                vBox.getChildren().add(gpuNameLabel);
                Label gpuVRamLabel = new Label("GPU " + i + " VRAM: " + gpus.get(i).getVram());
                vBox.getChildren().add(gpuVRamLabel);
            }
        }

        vBox.getChildren().add(hLineLabel4);
        vBox.getChildren().add(hardwareInfoLabel);

        if (hardwares.size() <= 0) {
            Label hardwareModelLabel = new Label("Диск не найден");
            vBox.getChildren().add(hardwareModelLabel);
        } else {
            for (int i = 0; i < hardwares.size(); i++) {
                Label hardwareModelLabel = new Label("Диск " + i + " model: " + hardwares.get(i).getModel());
                vBox.getChildren().add(hardwareModelLabel);
                Label hardwareNameLabel = new Label("Диск " + i + " name: " + hardwares.get(i).getName());
                vBox.getChildren().add(hardwareNameLabel);
                Label hardwareSizeLabel = new Label("Диск " + i + " size: " + hardwares.get(i).getSize());
                vBox.getChildren().add(hardwareSizeLabel);
            }
        }
        vBox.getChildren().add(new Label("Кучу кучу всего еще сюда можно засунуть..."));

        return vBox;
    }


    public static void main(String[] args) {
        launch(args);
    }
}